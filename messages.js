/* eslint-disable */
/* TODO eslint disable will be removed */
import baseEvents from '../helpers/enum/baseEvents';

module.exports = Object.freeze({
  composer: {
    invalidContext: {
      code: 1,
      message: 'composer.invalidContext',
    }
  },
  address: {
    notFound: {
      code: 1000,
      message: 'address.notFound',
    },
    validationFailed: {
      code: 1001,
      message: 'address.validationFailed',
    },
  },
  applyPromotion: {
    ok: {
      code: 14001,
      event: baseEvents.success,
      message: 'status.promotionApplied.successfully'
    },
    invalidStatus: {
      code: 14000,
      message: 'status.promotionApplied.invalid',
      event: baseEvents.badRequest
    }
  },
  cart: {
    ok: {
      code: 2000,
      message: 'cart.ok',
      event: baseEvents.success,
    },
    badRequest: {
      code: 2001,
      message: 'cart.badRequest',
      event: baseEvents.badRequest,
    },
    notFound: {
      code: 2002,
      message: 'cart.notFound',
      event: baseEvents.notFound,
    },
    unauthorized: {
      code: 2003,
      message: 'cart.unauthorized'
    },
    internalServerError: {
      code: 2004,
      message: 'cart.internalServerError'
    },
    invalidStatus: {
      code: 2005,
      message: 'cart.invalidStatus',
      event: baseEvents.badRequest,
    },
    concurrency: {
      code: 2006,
      message: 'cart.concurrencyError'
    },
    cartConverting: {
      code: 2007,
      event: baseEvents.badRequest,
      message: ''
    }
  },
  cartItem: {
    ok: {
      code: 3000,
      message: 'cartItem.ok',
      event: baseEvents.success,
    },
    invalidQuantity: {
      code: 3001,
      message: 'cartItem.invalidQuantity',
      event: baseEvents.badRequest,
    },
    invalidGift: {
      code: 3002,
      message: 'cartItem.invalidGift',
      event: baseEvents.badRequest,
    },
    unavailable: {
      code: 3003,
      message: 'cartItem.unavailable',
      event: baseEvents.success,
    },
    exceededQuantity: {
      code: 3004,
      message: 'cartItem.exceededQuantity',
    },
    notFound: {
      code: 3005,
      message: 'cartItem.notFound',
      event: baseEvents.notFound,
    },
    invalidGiftQuantity: {
      code: 3006,
      message: 'cartItem.gift.invalidQuantity',
    },
    productAlreadyInCart: {
      code: 3007,
      message: 'cartItem.productAlreadyInCart',
    },
    unknownGiftItem: {
      code: 3008,
      message: 'cartItem.gift.unknown',
    },
    unknownItemTypeId: {
      code: 3009,
      message: 'cartItem.itemType.unknown',
    },
    promotionNotFound: {
      code: 3010,
      message: 'cartItem.promotion.notFound',
    },
    validationFailed: {
      code: 3011,
      message: 'cartItem.validationFailed',
      event: baseEvents.badRequest,
    },
    internalServerError: {
      code: 3012,
      message: 'cartItem.internalServerError',
    },
    bundleValidationFailed: {
      code: 3013,
      message: 'cartItem.bundle.validationFailed',
      event: baseEvents.badRequest,
    },
    duplicatedRequest: {
      code: 3014,
      message: 'cartItem.duplicatedRequest',
      event: baseEvents.success,
    },
    productNotFound: {
      code: 3015,
      message: 'cartItem.product.notFound',
      event: baseEvents.success,
    },
    duplicatedProduct: {
      code: 3016,
      message: 'cartItem.duplicatedProduct',
      event: baseEvents.badRequest,
    },
    duplicatedQuantity: {
      code: 3017,
      message: 'cartItem.duplicatedQuantity',
      event: baseEvents.badRequest,
    }
  },
  checkout: {
    cartCreditBlock: {
      code: 5000,
      message: 'checkout.creditBlock',
    },
    cartDebitBlock: {
      code: 5001,
      message: 'checkout.debitBlock',
    },
    cartEmptyPurchasedItems: {
      code: 5019,
      message: 'checkout.cart.empty.purchased.items'
    },
    cartPersonBlock: {
      code: 5002,
      message: 'checkout.personBlock',
    },
    invalidPaymentAmount: {
      code: 5003,
      message: 'checkout.payment.invalid',
    },
    invalidDeliveryMode: {
      code: 5004,
      message: 'checkout.deliveryMode.invalid',
    },
    invalidGiftItem: {
      code: 5005,
      message: 'checkout.gift.invalid',
    },
    invalidDistinctItemQuantity: {
      code: 5006,
      message: 'checkout.choosableGift.distinctQuantity',
    },
    invalidGiftQuantity: {
      code: 5007,
      message: 'checkout.gift.invalidQuantity',
    },
    invalidChoosableGiftQuantity: {
      code: 5008,
      message: 'checkout.choosableGift.invalidQuantity',
    },
    shippingAddressNotFound: {
      code: 5009,
      message: 'checkout.address.notFound',
    },
    pendingCostCenter: {
      code: 5010,
      message: 'checkout.costCenter.missing',
    },
    pendingMinimumOrderSize: {
      code: 5011,
      message: 'checkout.minimumOrderSize',
    },
    pendingPaymentCondition: {
      code: 5012,
      message: 'checkout.paymentCondition.missing'
    },
    pendingPromotion: {
      code: 5013,
      message: 'checkout.promotion.pending',
    },
    pendingStarterKit: {
      code: 5014,
      message: 'checkout.starterkit.missing',
    },
    pendencies: {
      creditExceedsAvailable: {
        code: 4,
        message: 'credit.exceeds.available',
        origin: 'checkout',
      },
      debitPending: {
        code: 3,
        message: 'debit.pending',
        origin: 'checkout',
      },
      pendingStarterKit: {
        code: 5,
        message: 'starterkit.pending',
        origin: 'checkout',
      },
    },
    promotionNotFound: {
      code: 5015,
      message: 'checkout.promotion.notFound',
    },
    ok: {
      code: 5016,
      message: 'checkout.ok',
    },
    validatorError: {
      code: 5017,
      message: 'checkout.validatorError'
    },
    validatorNotFound: {
      code: 5018,
      message: 'checkout.validator.notFound'
    }
  },
  inactiveCarts: {
    invalidParameters: {
      code: 6000,
      message: 'inactiveCarts.invalidParameters',
    },
    processed: {
      code: 6001,
      message: 'inactiveCarts.ok',
    },
    unprocessed: {
      code: 6002,
      message: 'inactiveCarts.failed',
    },
  },
  costCenter: {
    ok: {
      code: 7000,
      message: 'costCenter.ok',
      event: baseEvents.success,
    },
    updateError: {
      code: 7001,
      message: 'costCenter.updateError',
    },
    notFound: {
      code: 7002,
      message: 'costCenter.notFound',
      event: baseEvents.notFound,
    },
    notFoundCode: {
      code: 7003,
      message: 'costCenter.paymentCondition.notFound',
      event: baseEvents.notFound,
    },
    notAllowed: {
      code: 7004,
      message: 'costCenter.notAllowed',
      event: baseEvents.success,
    }
  },
  payment: {
    notFound: {
      code: 8000,
      event: baseEvents.notFound,
      message: 'paymentMethod.notFound'
    },
    ok: {
      code: 8001,
      event: baseEvents.success,
      message: 'payment.ok'
    },
    costCenterRequired: {
      code: 8002,
      event: baseEvents.badRequest,
      message: 'payment.costCenter.required'
    },
  },
  incorporation: {
    notFound: {
      code: 9000,
      message: 'incorporation.notFound',
    },
    invalidQuantity: {
      code: 9001,
      message: 'incorporation.invalidQuantity',
    }
  },
  messageBus: {
    internalServerError: {
      code: 10000,
      message: 'messageBus.internalServerError'
    },
    content: {
      code: 10001,
      message: 'messageBus.content'
    }
  },
  shipping: {
    notFound: {
      event: baseEvents.badRequest,
      code: 11000,
      message: 'shipping.notFound'
    },
    success: {
      event: baseEvents.success,
      code: 11001,
      message: 'shipping.ok'
    },
    internalServerError: {
      code: 11002,
      message: 'shipping.internalServerError'
    },
  },
  availability: {
    unknownError: {
      code: 12000,
      message: 'availability.unknownError',
    },
    timeout: {
      code: 12001,
      message: 'availability.timeout',
    },
    commitmentNotFound: {
      code: 12002,
      message: 'availability.commitmentNotFound',
    }
  },
  token: {
    validationFailed: {
      code: 13000,
      message: 'token.validationFailed',
      event: baseEvents.badRequest,
    },
  },
  parameters: {
    timeout: {
      code: 14000,
      message: 'parameters.timeout',
    }
  },
  logger: {
    etimedout: 'ETIMEDOUT',
    services: {
      summarization: 'SummarizationService',
      availability: 'AvailabilityService',
      cart: 'CartService',
      cartMine: 'CartMineService',
      cartItem: 'CartItemService',
      cartPayment: 'CartPaymentService',
      shipping: 'ShippingService',
      parameters: 'ParametersService',
    },
    cartMine: {
      debug: 'using.data',
      error: 'internal.server.error',
      info: {
        start: 'get.cart.mine',
      },
      namespace: 'cartMine.command',
      success: {
        found: 'found.cart.with.id',
        new: 'created.cart.with.id ',
      },
    },
    checkout: {
      error: 'internal.server.error',
      invoked: 'checkout.invoked',
      namespace: 'checkout.command',
      success: 'checkout.success',
      warn: {
        badRequest: 'business.errors',
        notFound: 'cart.not.found',
      },
    },
    deleteCart: {
      error: 'internal.server.error',
      namespace: 'delete.cart.command',
      invoked: 'delete.cart.invoked',
      internalServerError: 'delete.cart.internalServerError',
      info: {
        start: 'delete.cart',
      },
      success: {
        found: 'found.cart.with.id',
        new: 'changes.status.to.disposed.cart.with.id',
      },
      warn: {
        badRequest: 'business.errors',
        notFound: 'cart.not.found',
      }
    },
    payment: {
      error: 'internal.server.error',
      info: {
        start: 'payment'
      },
      success: {
        new: 'changes.cart.payments'
      }
    },
    getCart: {
      namespace: 'get.cart.command',
      info: {
        start: 'get.cart'
      },
      invoked: 'get.cart.invoked',
      internalServerError: 'get.cart.internalServerError'
    },
    postItem: {
      namespace: 'postItem.command',
      internalServerError: 'postItem.internalServerError',
      summarization: {
        message: 'postItem.summarization.message',
      },
    },
    putItem: {
      namespace: 'putItem.command',
      invoked: 'putItem.invoked',
      internalServerError: 'putItem.internalServerError'
    },
    deleteItem: {
      namespace: 'deleteItem.command',
      userInfo: 'deleteItem.userInfo',
      internalServerError: 'deleteItem.internalServerError'
    },
    shipping: {
      namespace: 'shipping.command',
      error: 'shipping.internalServerError',

    },
    costCenter: {
      namespace: 'costCenter.command',
      costCenter: 'costCenter.internalServerError'
    }
  },
});